﻿using System;
using CypherFile;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetOperation_Dectypt()
        {
            try
            {
                FileCrypt f = new FileCrypt("a.aes", "PASS");
                Assert.AreEqual(f.GetOperation(), FileCrypt.Operation.DECRYPT);
            }
            catch (Exception e) {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void GetOperation_Encrypt()
        {
            try { 
                FileCrypt f = new FileCrypt("a.txt", "PASS");
                Assert.AreEqual(f.GetOperation(), FileCrypt.Operation.ENCRYPT);
            }
            catch (Exception e) {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void GetOperation_ShortName()
        {
            try { 
                FileCrypt f = new FileCrypt("a.t", "PASS");
                Assert.AreEqual(f.GetOperation(), FileCrypt.Operation.ENCRYPT);
            }
            catch (Exception e) {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void GetOperation_LongName()
        {
            try
            {
                FileCrypt f = new FileCrypt("a.pdf.aes", "PASS");
                Assert.AreEqual(f.GetOperation(), FileCrypt.Operation.DECRYPT);
            }
            catch (Exception e)
            {
                Assert.Fail();
            }
        }


    }
}
