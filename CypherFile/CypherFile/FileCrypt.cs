﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;


namespace CypherFile
{
    public class FileCrypt
    {
        [Flags]
        public enum Operation{
            ENCRYPT,
            DECRYPT
        };

        private string EXTENSION = ".aes";

        private string FileName, Password;

        AES aes;

        [DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory")]
        public static extern bool ZeroMemory(IntPtr Destination, int Length);

        public FileCrypt(string filename,string password)
        {
            FileName = filename.ToLower();
            Password = password;
            aes=new AES(EXTENSION);
        }

        public Operation GetOperation() {            
            if ((FileName.Length>= EXTENSION.Length))
            {                
                if ( (String.Compare(FileName.Substring(FileName.Length - 4, EXTENSION.Length), EXTENSION) == 0))
                return Operation.DECRYPT;
            }
            return Operation.ENCRYPT;
        }

        public void Encrypt()
        {
            GCHandle gCHandle = GCHandle.Alloc(Password, GCHandleType.Pinned);
            aes.FileEncrypt(FileName, Password);
            ZeroMemory(gCHandle.AddrOfPinnedObject(), Password.Length * 2);
            gCHandle.Free();
        }

        public void Decrypt()
        {
            GCHandle gch = GCHandle.Alloc(Password, GCHandleType.Pinned);
            aes.FileDecrypt(FileName, FileName.Substring(0, FileName.Length - 4), Password);
            ZeroMemory(gch.AddrOfPinnedObject(), Password.Length * 2);
            gch.Free();
        }

        public void Process() {    
            if (GetOperation() == Operation.ENCRYPT)
            {
                Encrypt();
            }else{
                Decrypt();
            }

            EraseOld();
        }

        static long GetFileSize(string FilePath)
        {
            if (File.Exists(FilePath))
            {
                return new FileInfo(FilePath).Length;
            }
            return 0;
        }

        public void EraseOld() {
            const int BuffSize = 1048576;
            long size = GetFileSize(FileName);

            using (FileStream f = new FileStream(FileName, FileMode.Create))
            {
                byte[] buffer = new byte[BuffSize];
                long i = 0;
                while ((long)i<size) 
                {
                    f.Write(buffer, 0, BuffSize);
                    i += BuffSize;
                    f.Flush();                    
                }
                f.Close();
                File.Delete(FileName);
            }            
        }
    }
}
