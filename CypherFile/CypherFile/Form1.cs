﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CypherFile
{
    public partial class Form1 : Form
    {   
        public Form1()
        {
            InitializeComponent();
            this.CenterToScreen();
        }       

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {                
                string[] args = Environment.GetCommandLineArgs();
                if (args.Length > 1)
                {
                    FileCrypt f = new FileCrypt(args[1], textBox1.Text.ToString());
                    this.Hide();
                    f.Process();
                }
                e.Handled = true;
                this.Close();
                return;
            }
            if (e.KeyCode == Keys.Escape) {
                e.Handled = true;
                this.Close();
                return;
            }
        }
    }
}
